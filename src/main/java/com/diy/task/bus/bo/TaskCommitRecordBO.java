package com.diy.task.bus.bo;

import com.diy.task.bus.entity.AnswerRecord;
import com.diy.task.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @ClassName com.diy.task.bus.bo.TaskCommitRecordBO.class
 * @Description TODO
 * @Author youwei.geyw
 * @Date 2019/3/10 9:42 AM
 * Version 1.0
 **/
@ApiModel(value = "TaskCommitRecordBO对象", description = "提交记录表")
public class TaskCommitRecordBO extends BaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "作业的提交人姓名")
    private String userName;

    @ApiModelProperty(value = "微信昵称")
    private String weixinNick;

    @ApiModelProperty(value = "题目ID")
    private String questionId;

    @ApiModelProperty(value = "提交的方式( 1 PC端  2 小程序端)")
    private Integer commitType;

    private List<AnswerRecord> answerRecordList;

    private Integer isDeleted;

    private Integer taskNumber;

    private String taskSubject;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWeixinNick() {
        return weixinNick;
    }

    public void setWeixinNick(String weixinNick) {
        this.weixinNick = weixinNick;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Integer getCommitType() {
        return commitType;
    }

    public void setCommitType(Integer commitType) {
        this.commitType = commitType;
    }

    public List<AnswerRecord> getAnswerRecordList() {
        return answerRecordList;
    }

    public void setAnswerRecordList(List<AnswerRecord> answerRecordList) {
        this.answerRecordList = answerRecordList;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(Integer taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskSubject() {
        return taskSubject;
    }

    public void setTaskSubject(String taskSubject) {
        this.taskSubject = taskSubject;
    }

    @Override
    public String toString() {
        return "TaskCommitRecord{" +
                "userId=" + userId +
                ", userName=" + userName +
                ", weixinNick=" + weixinNick +
                ", questionId=" + questionId +
                ", commitType=" + commitType +
                "}";
    }

}
