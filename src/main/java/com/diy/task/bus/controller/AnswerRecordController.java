package com.diy.task.bus.controller;


import com.diy.task.bus.entity.AnswerRecord;
import com.diy.task.bus.service.IAnswerRecordService;
import com.diy.task.common.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 答案记录表 前端控制器
 * </p>
 *
 * @author gao
 * @since 2019-03-05
 */
@Controller
@RequestMapping("/bus/answer-record")
public class AnswerRecordController extends BaseController<IAnswerRecordService, AnswerRecord> {

}
