package com.diy.task.bus.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.diy.task.common.base.BaseController;
import com.diy.task.bus.service.IQuestionContentService;
import com.diy.task.bus.entity.QuestionContent;

/**
 * <p>
 * 问题记录表 前端控制器
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
@Controller
@RequestMapping("/bus/question-content")
public class QuestionContentController extends BaseController<IQuestionContentService,QuestionContent> {

}
