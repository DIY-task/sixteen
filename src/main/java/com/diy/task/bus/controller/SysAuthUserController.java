package com.diy.task.bus.controller;


import com.diy.task.bus.entity.SysAuthUser;
import com.diy.task.bus.service.ISysAuthUserService;
import com.diy.task.common.base.BaseController;
import com.diy.task.common.base.BaseResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author youwei.gewy
 * @since 2019-03-10
 */
@Controller
@RequestMapping("/bus/sys-auth-user")
public class SysAuthUserController extends BaseController<ISysAuthUserService, SysAuthUser> {

    @Autowired
    ISysAuthUserService iSysAuthUserService;

    /**
     * 查询所有用户列表
     *
     * @param
     * @return
     */
    @ApiOperation(value = "查询所有用户列表", notes = "code：0 找到记录，1未找到记录")
    @GetMapping("listUserAll")
    @ResponseBody
    public BaseResult listUserAll() {
        List<SysAuthUser> sysAuthUserList = iSysAuthUserService.listUserAll();
        return BaseResult.ok(sysAuthUserList);
    }


    /**
     * 分页查询用户列表
     *
     * @param
     * @return
     */
    @ApiOperation(value = "分页查询用户列表", notes = "code：0 找到记录，1未找到记录")
    @GetMapping("listUserByPage")
    @ResponseBody
    public BaseResult listUserByPage() {
        List<SysAuthUser> sysAuthUserList = iSysAuthUserService.listUserAll();
        return BaseResult.ok(sysAuthUserList);
    }

    /**
     * 新增用户
     *
     * @param
     * @return
     */
    @ApiOperation(value = "新增用户", notes = "code：0 找到记录，1未找到记录")
    @GetMapping("addUser")
    @ResponseBody
    public BaseResult addUser() {
        SysAuthUser sysAuthUser = new SysAuthUser();
        iSysAuthUserService.addUser(sysAuthUser);
        return BaseResult.ok();
    }

    /**
     * 更新用户
     *
     * @param
     * @return
     */
    @ApiOperation(value = "更新用户", notes = "code：0 找到记录，1未找到记录")
    @GetMapping("updateUser")
    @ResponseBody
    public BaseResult updateUser() {
        SysAuthUser sysAuthUser = new SysAuthUser();
        iSysAuthUserService.updateUser(sysAuthUser);
        return BaseResult.ok();
    }


    /**
     * 删除用户
     *
     * @param
     * @return
     */
    @ApiOperation(value = "删除用户", notes = "code：0 找到记录，1未找到记录")
    @GetMapping("deleteUser")
    @ResponseBody
    public BaseResult deleteUser() {
        SysAuthUser sysAuthUser = new SysAuthUser();
        iSysAuthUserService.deleteUser(sysAuthUser);
        return BaseResult.ok();
    }
}
