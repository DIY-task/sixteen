package com.diy.task.bus.controller;


import com.diy.task.bus.bo.TaskCommitRecordBO;
import com.diy.task.bus.entity.TaskCommitRecord;
import com.diy.task.bus.entity.TaskContent;
import com.diy.task.bus.entity.query.TaskCommitRecordInfoQuery;
import com.diy.task.bus.query.TaskCommitRecordQuery;
import com.diy.task.bus.service.ITaskCommitRecordService;
import com.diy.task.common.base.BaseController;
import com.diy.task.common.base.BaseResult;
import com.diy.task.common.base.SmartPage;
import com.diy.task.sys.entity.AuthUser;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 提交记录表 前端控制器
 * </p>
 *
 * @author youwei
 * @since 2019-03-07
 */
@Controller
@RequestMapping("/bus/task-commit-record")
public class TaskCommitRecordController extends BaseController<ITaskCommitRecordService, TaskCommitRecord> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskContentController.class);

    @Autowired
    ITaskCommitRecordService iTaskCommitRecordService;

    /**
     * 获取提交作业列表，带回答详情
     *
     * @param SmartPage spage 示例：{"search":{"userName":"高","taskContentId":"1","beginDate":"","endDate":""},"ascs":[],"descs":[],"size":2,"current":1}
     * @return 返回查找结果,没有则返回null，前端注意判空
     */
    @ApiOperation(value = "获取提交作业列表，带回答详情", notes = "传入参数：search查询条件实体，ascs升序条件，descs降序条件，size每页大小，current当前页。返回查找结果,没有则返回null，前端注意判空")
    @PostMapping("commitInfoList")
    @ResponseBody
    public BaseResult getCommitInfoListByPage(@RequestBody SmartPage<TaskCommitRecordInfoQuery> spage) {
        return BaseResult.ok(service.getTaskCommitRecordInfoList(spage));
    }

    /**
     * 获取话题内容和题目的列表页
     *
     * @param
     * @return
     */
    @ApiOperation(value = "获取话题内容和题目的列表页", notes = "code：0 找到记录，1未找到记录")
    @GetMapping("listByPage")
    @ResponseBody
    public BaseResult listByPage(TaskCommitRecordQuery taskCommitRecordQuery) {
        List<TaskCommitRecordBO> taskCommitRecordBOList = (List<TaskCommitRecordBO>) iTaskCommitRecordService.listByPage(taskCommitRecordQuery);
        if (!CollectionUtils.isEmpty(taskCommitRecordBOList)) {
            taskCommitRecordBOList.stream().forEach(taskCommitRecordBO -> {

            });
        }

        return BaseResult.ok();
    }

    /**
     * 提交作业
     *
     * @param taskCommitRecord
     * @return
     */
    @ApiOperation(value = "提交作业", notes = "code：0 提交作业成功")
    @PostMapping("addCommitRecord")
    @ResponseBody
    public BaseResult addCommitRecord(@RequestBody TaskCommitRecord taskCommitRecord, @ModelAttribute AuthUser authUser) {
        try {
            LOGGER.info("addCommitRecord方法已执行，参数{}", taskCommitRecord);
//            String userName = authUser.getUserName();
            taskCommitRecord.setCreator(taskCommitRecord.getUserId());
            taskCommitRecord.setModifier(taskCommitRecord.getUserId());
            iTaskCommitRecordService.saveTaskCommitRecord(taskCommitRecord);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseResult.ok("提交作业成功");
    }

    /**
     * 编辑作业
     *
     * @param taskCommitRecord
     * @return
     */
    @ApiOperation(value = "编辑作业", notes = "code：0 编辑作业成功")
    @PostMapping("updateCommitRecord")
    @ResponseBody
    public BaseResult updateCommitRecord(@RequestBody TaskCommitRecord taskCommitRecord, @ModelAttribute AuthUser authUser) {
        LOGGER.info("updateCommitRecord方法已执行，参数{}", taskCommitRecord);
        String userName = authUser.getUserName();
        taskCommitRecord.setModifier(userName);
        iTaskCommitRecordService.updateTaskCommitRecord(taskCommitRecord);
        return BaseResult.ok("编辑作业成功");
    }

    /**
     * 展示当前添加作业的主题和所有问题
     *
     * @return
     */
    @ApiOperation(value = "展示当前添加作业的主题和所有问题", notes = "code：0查询成功")
    @PostMapping("selectTaskContent")
    @ResponseBody
    public BaseResult selectTaskContent() {
        LOGGER.info("selectTaskContent方法已执行");
        TaskContent taskContent = iTaskCommitRecordService.selectTaskContent();
        return BaseResult.ok(taskContent);
    }


    /**
     * 展示答案的详情内容
     *
     * @return
     */
    @ApiOperation(value = "展示答案的详情内容", notes = "code：0查询成功")
    @GetMapping("getDetail/{id}")
    @ResponseBody
    public BaseResult getDetailTaskContent(@PathVariable String id) {
        LOGGER.info("getDetailTaskContent方法已执行");
        TaskCommitRecord taskCommitRecord = iTaskCommitRecordService.getTaskCommitRecord(id);
        return BaseResult.ok(taskCommitRecord);
    }
}
