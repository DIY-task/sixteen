package com.diy.task.bus.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.diy.task.bus.entity.TaskContent;
import com.diy.task.bus.service.ITaskContentService;
import com.diy.task.common.base.BaseController;
import com.diy.task.common.base.BaseResult;
import com.diy.task.sys.entity.AuthUser;
import com.diy.task.sys.service.IAuthUserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 话题内容表 前端控制器
 * </p>
 * @author nengjie
 * @since 2019-03-05
 */
@Controller
@RequestMapping("/bus/task-content")
public class TaskContentController extends BaseController<ITaskContentService,TaskContent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskContentController.class);

    @Autowired
    private ITaskContentService iTaskContentService;

    @Autowired
    private IAuthUserService iAuthUserService;


    /**
     * 根据id获取话题内容详情表
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id获取话题内容详情表" , notes = "code：0 找到记录，1未找到记录")
    @GetMapping("getDetail/{id}")
    @ResponseBody
    public BaseResult getDetail(@PathVariable String id) {
        LOGGER.info("getDetail方法已执行，参数{}",id);
        TaskContent taskContent = iTaskContentService.getDetail(id);
        return taskContent == null ? BaseResult.error("未找到记录"): BaseResult.ok(taskContent);
    }


    /**
     * 获取话题内容和题目的列表页
     * @param
     * @return
     */
    @ApiOperation(value = "获取话题内容和题目的列表页" , notes = "code：0 找到记录，1未找到记录")
    @GetMapping("getAppletsList")
    @ResponseBody
    public BaseResult getAppletsList(@RequestParam(defaultValue = "1") Long page , @RequestParam(defaultValue = "10") Long limit,
                                     @RequestBody(required = false) TaskContent taskContent) {
        LOGGER.info("getAppletsList方法已执行，参数{}",taskContent);
        IPage<TaskContent> taskContentList = iTaskContentService.getAppletsList(taskContent,page,limit);
        return BaseResult.ok(taskContentList);
    }

    /**
     * 添加话题内容
     * @param taskContent
     * @return
     */
    @ApiOperation(value = "添加话题内容" , notes = "code：0 添加话题成功")
    @PostMapping("addTaskContent")
    @ResponseBody
    public BaseResult addTaskContent(@RequestBody TaskContent taskContent, @ModelAttribute AuthUser authUser) {
        LOGGER.info("addTaskContent方法已执行，参数{}",taskContent);
        String userName = authUser.getUserName();
        taskContent.setCreator(userName);
        taskContent.setModifier(userName);
        iTaskContentService.saveTaskContent(taskContent);
        return BaseResult.ok("添加话题成功");
    }

    /**
     * 编辑话题内容
     * @param taskContent
     * @return
     */
    @ApiOperation(value = "编辑话题内容" , notes = "code：0 编辑话题成功")
    @PostMapping("updateTaskContent")
    @ResponseBody
    public BaseResult updateTaskContent(@RequestBody TaskContent taskContent,@ModelAttribute AuthUser authUser) {
        LOGGER.info("updateTaskContent方法已执行，参数{}",taskContent);
        String userName = authUser.getUserName();
        taskContent.setCreator(userName);
        taskContent.setModifier(userName);
        iTaskContentService.updateTaskContent(taskContent);
        return BaseResult.ok("编辑话题成功");
    }

    /**
     * 导出未按时提交作业的人员名单
     * @return
     */
    @ApiOperation(value = "导出未按时提交的人员名单" , notes = "下载保存文件即可")
    @GetMapping("export")
    public void export(HttpServletResponse httpServletResponse) {
        LOGGER.info("export方法已执行");
        String fileName = "未提交的学员名单";
        iAuthUserService.export(httpServletResponse,fileName);
    }
}
