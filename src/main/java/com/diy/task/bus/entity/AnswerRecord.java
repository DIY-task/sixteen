package com.diy.task.bus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.diy.task.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 答案记录表
 * </p>
 *
 * @author gao
 * @since 2019-03-05
 */
@TableName("bus_answer_record")
@ApiModel(value="AnswerRecord对象", description="答案记录表")
public class AnswerRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "答案内容")
    private String answerContext;

    @ApiModelProperty(value = "提交记录表ID")
    private String taskCommitId;

    /**
     * 题目的id
     */
    @TableField(value = "question_id")
    private String questionId;

    /**
     * 题目的内容
     */
    @TableField(exist = false)
    private String questionContent;

    public String getAnswerContext() {
        return answerContext;
    }

    public void setAnswerContext(String answerContext) {
        this.answerContext = answerContext;
    }

    public String getTaskCommitId() {
        return taskCommitId;
    }

    public void setTaskCommitId(String taskCommitId) {
        this.taskCommitId = taskCommitId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    @Override
    public String toString() {
        return "AnswerRecord{" +
                "answerContext='" + answerContext + '\'' +
                ", taskCommitId=" + taskCommitId +
                '}';
    }
}
