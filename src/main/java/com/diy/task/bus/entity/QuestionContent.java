package com.diy.task.bus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.diy.task.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 问题记录表
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
@TableName("bus_question_content")
@ApiModel(value="QuestionContent对象", description="问题记录表")
public class QuestionContent extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "作业内容")
    private String questionContent;

    @ApiModelProperty(value = "话题内容表ID")
    private String taskContentId;

    @ApiModelProperty(value = "问题排序")
    private int questionOrder;

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getTaskContentId() {
        return taskContentId;
    }

    public void setTaskContentId(String taskContentId) {
        this.taskContentId = taskContentId;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }

    @Override
    public String toString() {
        return "QuestionContent{" +
                "questionContent='" + questionContent + '\'' +
                ", taskContentId='" + taskContentId + '\'' +
                ", questionOrder='" + questionOrder + '\'' +
                '}';
    }
}
