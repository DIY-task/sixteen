package com.diy.task.bus.entity;

import com.diy.task.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author youwei.gewy
 * @since 2019-03-10
 */
@ApiModel(value="SysAuthUser对象", description="用户表")
public class SysAuthUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @ApiModelProperty(value = "用户姓名")
    private String userName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "性别(1男 2女)")
    private Integer userSex;

    @ApiModelProperty(value = "提交作业的期数")
    private Integer commitNumber;

    @ApiModelProperty(value = "NORMAL-正常  DELETED 停用 INITIAL初始化 AUTO_LOCKED 锁定")
    private String status;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public Integer getUserSex() {
        return userSex;
    }

    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }
    public Integer getCommitNumber() {
        return commitNumber;
    }

    public void setCommitNumber(Integer commitNumber) {
        this.commitNumber = commitNumber;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SysAuthUser{" +
        "roleId=" + roleId +
        ", userName=" + userName +
        ", password=" + password +
        ", userSex=" + userSex +
        ", commitNumber=" + commitNumber +
        ", status=" + status +
        "}";
    }
}
