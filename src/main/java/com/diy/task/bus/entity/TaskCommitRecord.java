package com.diy.task.bus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.diy.task.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 提交记录表
 * </p>
 *
 * @author youwei
 * @since 2019-03-07
 */
@TableName("bus_task_commit_record")
@ApiModel(value = "TaskCommitRecord对象", description = "提交记录表")
public class TaskCommitRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "作业的提交人姓名")
    private String userName;

    @ApiModelProperty(value = "微信昵称")
    private String weixinNick;

    @ApiModelProperty(value = "话题内容表ID")
    private String taskContentId;

    @ApiModelProperty(value = "提交的方式( 1 PC端  2 小程序端)")
    private Integer commitType;

    @ApiModelProperty(value = "提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime commitDate;

    @TableField(exist = false)
    private List answerRecordList;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWeixinNick() {
        return weixinNick;
    }

    public void setWeixinNick(String weixinNick) {
        this.weixinNick = weixinNick;
    }

    public String getTaskContentId() {
        return taskContentId;
    }

    public void setTaskContentId(String taskContentId) {
        this.taskContentId = taskContentId;
    }

    public Integer getCommitType() {
        return commitType;
    }

    public void setCommitType(Integer commitType) {
        this.commitType = commitType;
    }

    public List getAnswerRecordList() {
        return answerRecordList;
    }

    public void setAnswerRecordList(List answerRecordList) {
        this.answerRecordList = answerRecordList;
    }

    public LocalDateTime getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(LocalDateTime commitDate) {
        this.commitDate = commitDate;
    }

    @Override
    public String toString() {
        return "TaskCommitRecord{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", weixinNick='" + weixinNick + '\'' +
                ", taskContentId='" + taskContentId + '\'' +
                ", commitType=" + commitType +
                ", commitDate=" + commitDate +
                ", answerRecordList=" + answerRecordList +
                '}';
    }
}
