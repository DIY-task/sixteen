package com.diy.task.bus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.diy.task.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 话题内容表
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
@TableName("bus_task_content")
@ApiModel(value = "TaskContent对象", description = "话题内容表")
public class TaskContent extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "作业期数")
    private String taskNumber;

    @ApiModelProperty(value = "作业主题")
    private String taskSubject;

    @ApiModelProperty(value = "作业提交截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime beginDate;

    @ApiModelProperty(value = "作业提交截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;


    @TableField(exist = false)
    private List<QuestionContent> questionContentList;


    /**
     * 状态描述
     */
    @TableField(exist = false)
    private String status;

    /**
     * 状态码
     */
    @TableField(exist = false)
    private Integer statusCode;


    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskSubject() {
        return taskSubject;
    }

    public void setTaskSubject(String taskSubject) {
        this.taskSubject = taskSubject;
    }

    public LocalDateTime getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDateTime beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<QuestionContent> getQuestionContentList() {
        return questionContentList;
    }

    public void setQuestionContentList(List<QuestionContent> questionContentList) {
        this.questionContentList = questionContentList;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "TaskContent{" +
                "taskNumber='" + taskNumber + '\'' +
                ", taskSubject='" + taskSubject + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", questionContentList=" + questionContentList +
                ", status='" + status + '\'' +
                ", statusCode='" + statusCode + '\'' +
                '}';
    }
}
