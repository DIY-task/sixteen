package com.diy.task.bus.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.diy.task.common.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Usage: 回答查询后的封装
 * @Author: gjq
 * @Date: 2019/3/17 4:14 PM
 */
public class AnswerRecordDTO extends BaseEntity {

    @ApiModelProperty(value = "答案内容")
    private String answerContext;

    @ApiModelProperty(value = "提交记录表ID")
    private String taskCommitId;

    @ApiModelProperty(value = "问题ID")
    private String questionId;

    @ApiModelProperty(value = "问题排序")
    private String questionContent;

    @ApiModelProperty(value = "问题排序")
    private String questionOrder;

    public String getAnswerContext() {
        return answerContext;
    }

    public void setAnswerContext(String answerContext) {
        this.answerContext = answerContext;
    }

    public String getTaskCommitId() {
        return taskCommitId;
    }

    public void setTaskCommitId(String taskCommitId) {
        this.taskCommitId = taskCommitId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(String questionOrder) {
        this.questionOrder = questionOrder;
    }

    @Override
    public String toString() {
        return "AnswerRecord{" +
                "answerContext='" + answerContext + '\'' +
                ", taskCommitId=" + taskCommitId +
                '}';
    }
}
