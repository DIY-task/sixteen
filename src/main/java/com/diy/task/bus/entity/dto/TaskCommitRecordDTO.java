package com.diy.task.bus.entity.dto;

import com.diy.task.bus.entity.AnswerRecord;
import com.diy.task.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Usage: 用于查询用户提交的基础封装
 * @Author: gjq
 * @Date: 2019/3/17 1:56 PM
 */
public class TaskCommitRecordDTO extends BaseEntity {
    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "作业的提交人姓名")
    private String userName;

    @ApiModelProperty(value = "话题内容表ID")
    private String taskContentId;

    @ApiModelProperty(value = "作业期数")
    private String taskNumber;

    @ApiModelProperty(value = "作业主题")
    private String taskSubject;

    @ApiModelProperty(value = "提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime commitDate;

    private List<AnswerRecordDTO> answerRecordList;

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskSubject() {
        return taskSubject;
    }

    public void setTaskSubject(String taskSubject) {
        this.taskSubject = taskSubject;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTaskContentId() {
        return taskContentId;
    }

    public void setTaskContentId(String taskContentId) {
        this.taskContentId = taskContentId;
    }

    public List<AnswerRecordDTO> getAnswerRecordList() {
        return answerRecordList;
    }

    public void setAnswerRecordList(List<AnswerRecordDTO> answerRecordList) {
        this.answerRecordList = answerRecordList;
    }

    public LocalDateTime getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(LocalDateTime commitDate) {
        this.commitDate = commitDate;
    }

    @Override
    public String toString() {
        return "TaskCommitRecordDTO{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", taskContentId='" + taskContentId + '\'' +
                ", taskNumber='" + taskNumber + '\'' +
                ", taskSubject='" + taskSubject + '\'' +
                ", commitDate=" + commitDate +
                ", answerRecordList=" + answerRecordList +
                '}';
    }
}
