package com.diy.task.bus.entity.query;

import java.time.LocalDateTime;

/**
 * @Usage:
 * @Author: gjq
 * @Date: 2019/3/17 5:25 PM
 */
public class TaskCommitRecordInfoQuery {
    private String userName;
    private String taskContentId;
    private String questionContentId;
    private LocalDateTime beginDate;
    private LocalDateTime endDate;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTaskContentId() {
        return taskContentId;
    }

    public void setTaskContentId(String taskContentId) {
        this.taskContentId = taskContentId;
    }

    public LocalDateTime getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDateTime beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getQuestionContentId() {
        return questionContentId;
    }

    public void setQuestionContentId(String questionContentId) {
        this.questionContentId = questionContentId;
    }
}
