package com.diy.task.bus.mapper;

import com.diy.task.bus.entity.AnswerRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 答案记录表 Mapper 接口
 * </p>
 *
 * @author gao
 * @since 2019-03-05
 */
public interface AnswerRecordMapper extends BaseMapper<AnswerRecord> {

}
