package com.diy.task.bus.mapper;

import com.diy.task.bus.entity.QuestionContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 问题记录表 Mapper 接口
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
public interface QuestionContentMapper extends BaseMapper<QuestionContent> {

}
