package com.diy.task.bus.mapper;

import com.diy.task.bus.entity.SysAuthUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author youwei.gewy
 * @since 2019-03-10
 */
public interface SysAuthUserMapper extends BaseMapper<SysAuthUser> {

}
