package com.diy.task.bus.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.diy.task.bus.bo.TaskCommitRecordBO;
import com.diy.task.bus.entity.TaskCommitRecord;
import com.diy.task.bus.entity.dto.AnswerRecordDTO;
import com.diy.task.bus.entity.dto.TaskCommitRecordDTO;
import com.diy.task.bus.entity.query.TaskCommitRecordInfoQuery;
import com.diy.task.common.dal.Condition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 提交记录表 Mapper 接口
 * </p>
 *
 * @author youwei
 * @since 2019-03-07
 */
public interface TaskCommitRecordMapper extends BaseMapper<TaskCommitRecord> {


    /**
     * 分页查询提价记录
     *
     * @param condition
     * @return
     */
    List<TaskCommitRecordBO> listTaskCommitRecordPageWithSubject(Condition condition);

    /**
     * 提交记录数量
     *
     * @param condition
     * @return
     */
    int listTaskCommitRecordCountWithSubject(Condition condition);

    /**
     * 获取提交内容
     * @param queryWrapper
     * @return
     */
    IPage<TaskCommitRecordDTO> selectTaskCommitList(IPage page, @Param("ew") QueryWrapper<TaskCommitRecordInfoQuery> queryWrapper);

    /**
     * 获取提交内容ID集合
     * @param queryWrapper
     * @return
     */
    IPage<String> selectTaskCommitListId(IPage page, @Param("ew") QueryWrapper<TaskCommitRecordInfoQuery> queryWrapper);

    /**
     * 获取一批提交记录的回答内容
     * @param array
     * @return
     */
    List<AnswerRecordDTO> selectAnswersByCommitList(List<String> array);


}
