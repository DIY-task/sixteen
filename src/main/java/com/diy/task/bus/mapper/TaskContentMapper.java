package com.diy.task.bus.mapper;

import com.diy.task.bus.entity.TaskContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 话题内容表 Mapper 接口
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
public interface TaskContentMapper extends BaseMapper<TaskContent> {

}
