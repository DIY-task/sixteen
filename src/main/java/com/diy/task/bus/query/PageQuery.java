package com.diy.task.bus.query;

import java.io.Serializable;

/**
 * 分页查询 基础类
 * @author youwei.geyw
 */
public  class PageQuery implements Serializable {
    private static final long serialVersionUID = 1110751969497268006L;

    private final static int DEFAULT_PAGE_INDEX = 1;

    private final static int DEFAULT_PAGE_SIZE = 20;

    private int pageIndex = DEFAULT_PAGE_INDEX;

    private int pageSize = DEFAULT_PAGE_SIZE;

    public int getPageIndex() {
        return pageIndex > 1 ? pageIndex : DEFAULT_PAGE_INDEX;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize > 0 ? pageSize : DEFAULT_PAGE_SIZE;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getOffset() {
        return (getPageIndex() - 1) * getPageSize();
    }

}
