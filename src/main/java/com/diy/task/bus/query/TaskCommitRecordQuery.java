package com.diy.task.bus.query;

import com.diy.task.bus.entity.AnswerRecord;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @ClassName com.diy.task.bus.query.TaskCommitRecordInfoQuery.class
 * @Description TODO
 * @Author youwei.geyw
 * @Date 2019/3/10 9:56 AM
 * Version 1.0
 **/
public class TaskCommitRecordQuery extends PageQuery {

    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty(value = "删除标志（0未删除，1已删除）")
    private Integer deleted;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "创建用户")
    private String creator;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "修改用户")
    private String modifier;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "作业的提交人姓名")
    private String userName;

    @ApiModelProperty(value = "微信昵称")
    private String weixinNick;

    @ApiModelProperty(value = "题目ID")
    private String questionId;

    @ApiModelProperty(value = "提交的方式( 1 PC端  2 小程序端)")
    private Integer commitType;

    private List<AnswerRecord> answerRecordList;

    private Integer isDeleted;

    private Integer taskNumber;

    private String taskSubject;

    private Date subjectStartTime;

    private Date subjectEndTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(LocalDateTime gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public LocalDateTime getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(LocalDateTime gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWeixinNick() {
        return weixinNick;
    }

    public void setWeixinNick(String weixinNick) {
        this.weixinNick = weixinNick;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Integer getCommitType() {
        return commitType;
    }

    public void setCommitType(Integer commitType) {
        this.commitType = commitType;
    }

    public List<AnswerRecord> getAnswerRecordList() {
        return answerRecordList;
    }

    public void setAnswerRecordList(List<AnswerRecord> answerRecordList) {
        this.answerRecordList = answerRecordList;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(Integer taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskSubject() {
        return taskSubject;
    }

    public void setTaskSubject(String taskSubject) {
        this.taskSubject = taskSubject;
    }

    public Date getSubjectStartTime() {
        return subjectStartTime;
    }

    public void setSubjectStartTime(Date subjectStartTime) {
        this.subjectStartTime = subjectStartTime;
    }

    public Date getSubjectEndTime() {
        return subjectEndTime;
    }

    public void setSubjectEndTime(Date subjectEndTime) {
        this.subjectEndTime = subjectEndTime;
    }

    @Override
    public String toString() {
        return "TaskCommitRecord{" +
                "userId=" + userId +
                ", userName=" + userName +
                ", weixinNick=" + weixinNick +
                ", questionId=" + questionId +
                ", commitType=" + commitType +
                "}";
    }

}
