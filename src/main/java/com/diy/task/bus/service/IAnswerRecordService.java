package com.diy.task.bus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.diy.task.bus.entity.AnswerRecord;

/**
 * <p>
 * 答案记录表 服务类
 * </p>
 *
 * @author gao
 * @since 2019-03-05
 */
public interface IAnswerRecordService extends IService<AnswerRecord> {




}
