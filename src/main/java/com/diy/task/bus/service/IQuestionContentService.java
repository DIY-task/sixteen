package com.diy.task.bus.service;

import com.diy.task.bus.entity.QuestionContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 问题记录表 服务类
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
public interface IQuestionContentService extends IService<QuestionContent> {

}
