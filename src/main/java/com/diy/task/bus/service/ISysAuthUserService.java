package com.diy.task.bus.service;

import com.diy.task.bus.entity.SysAuthUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author youwei.gewy
 * @since 2019-03-10
 */
public interface ISysAuthUserService extends IService<SysAuthUser> {


    /**
     * 查询所有用户列表
     *
     * @return
     */
    List<SysAuthUser> listUserAll();


    /**
     * 新增用户
     *
     * @param sysAuthUser
     */
    void addUser(SysAuthUser sysAuthUser);


    /**
     * 更新用户
     *
     * @param sysAuthUser
     */
    void updateUser(SysAuthUser sysAuthUser);


    /**
     * 删除用户
     *
     * @param sysAuthUser
     */
    void deleteUser(SysAuthUser sysAuthUser);
}
