package com.diy.task.bus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.diy.task.bus.bo.TaskCommitRecordBO;
import com.diy.task.bus.entity.TaskCommitRecord;
import com.diy.task.bus.entity.TaskContent;
import com.diy.task.bus.entity.dto.TaskCommitRecordDTO;
import com.diy.task.bus.entity.query.TaskCommitRecordInfoQuery;
import com.diy.task.bus.query.TaskCommitRecordQuery;
import com.diy.task.common.base.SmartPage;

import java.util.List;

/**
 * <p>
 * 提交记录表 服务类
 * </p>
 *
 * @author youwei
 * @since 2019-03-07
 */
public interface ITaskCommitRecordService extends IService<TaskCommitRecord> {


    /**
     * 获取提交记录列表，带题目回答详情
     * @param spage
     * @return
     */
    List<TaskCommitRecordDTO> getTaskCommitRecordInfoList(SmartPage<TaskCommitRecordInfoQuery> spage);

    /**
     * 提交记录分页查询
     *
     * @param taskCommitRecordQuery
     * @return
     */
    IPage<TaskCommitRecordBO> listByPage(TaskCommitRecordQuery taskCommitRecordQuery);

    /**
     * 新增提交记录
     *
     * @param taskCommitRecord
     */
    void saveTaskCommitRecord(TaskCommitRecord taskCommitRecord);

    /**
     * 更新提交记录
     *
     * @param taskCommitRecord
     */
    void updateTaskCommitRecord(TaskCommitRecord taskCommitRecord);

    /**
     * 根据ID获取提交记录
     *
     * @param id
     * @return
     */
    TaskCommitRecord getTaskCommitRecord(String id);

    /**
     * 展示当前添加作业的主题和所有问题
     * @return
     */
    TaskContent selectTaskContent();

}
