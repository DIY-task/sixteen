package com.diy.task.bus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.diy.task.bus.entity.TaskContent;

/**
 * <p>
 * 话题内容表 服务类
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
public interface ITaskContentService extends IService<TaskContent> {

    /**
     * 根据ID获取话题详情表
     *
     * @param id
     * @return
     */
    TaskContent getDetail(String id);

    /**
     * 添加话题内容
     *
     * @param taskContent
     */
    void saveTaskContent(TaskContent taskContent);

    /**
     * 编辑话题内容
     *
     * @param taskContent
     */
    void updateTaskContent(TaskContent taskContent);

    /**
     * 获取小程序端话题列表页面
     *
     * @param taskContent
     * @param page
     * @param limit
     * @return
     */
    IPage<TaskContent> getAppletsList(TaskContent taskContent, Long page, Long limit);
}
