package com.diy.task.bus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.diy.task.bus.entity.AnswerRecord;
import com.diy.task.bus.mapper.AnswerRecordMapper;
import com.diy.task.bus.service.IAnswerRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 答案记录表 服务实现类
 * </p>
 *
 * @author gao
 * @since 2019-03-05
 */
@Service
public class AnswerRecordServiceImpl extends ServiceImpl<AnswerRecordMapper, AnswerRecord> implements IAnswerRecordService {


}
