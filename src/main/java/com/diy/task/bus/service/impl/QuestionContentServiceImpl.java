package com.diy.task.bus.service.impl;

import com.diy.task.bus.entity.QuestionContent;
import com.diy.task.bus.mapper.QuestionContentMapper;
import com.diy.task.bus.service.IQuestionContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 问题记录表 服务实现类
 * </p>
 *
 * @author nengjie
 * @since 2019-03-05
 */
@Service
public class QuestionContentServiceImpl extends ServiceImpl<QuestionContentMapper, QuestionContent> implements IQuestionContentService {

}
