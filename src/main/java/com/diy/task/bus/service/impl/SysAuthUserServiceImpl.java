package com.diy.task.bus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.diy.task.bus.entity.SysAuthUser;
import com.diy.task.bus.mapper.SysAuthUserMapper;
import com.diy.task.bus.service.ISysAuthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author youwei.gewy
 * @since 2019-03-10
 */
@Service
public class SysAuthUserServiceImpl extends ServiceImpl<SysAuthUserMapper, SysAuthUser> implements ISysAuthUserService {


    @Autowired
    private SysAuthUserMapper sysAuthUserMapper;

    @Override
    public List<SysAuthUser> listUserAll() {
        QueryWrapper<SysAuthUser> sysAuthUserQueryWrapper = new QueryWrapper<>();
        sysAuthUserQueryWrapper.eq("is_deleted", 0).orderBy(true, false, "id");
        return sysAuthUserMapper.selectList(sysAuthUserQueryWrapper);
    }

    @Override
    public void addUser(SysAuthUser sysAuthUser) {
        sysAuthUserMapper.insert(sysAuthUser);
    }

    @Override
    public void updateUser(SysAuthUser sysAuthUser) {
        sysAuthUserMapper.updateById(sysAuthUser);

    }

    @Override
    public void deleteUser(SysAuthUser sysAuthUser) {
        sysAuthUserMapper.deleteById(sysAuthUser.getId());
    }
}
