package com.diy.task.bus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.diy.task.bus.bo.TaskCommitRecordBO;
import com.diy.task.bus.entity.AnswerRecord;
import com.diy.task.bus.entity.TaskCommitRecord;
import com.diy.task.bus.entity.dto.AnswerRecordDTO;
import com.diy.task.bus.entity.dto.TaskCommitRecordDTO;
import com.diy.task.bus.entity.query.TaskCommitRecordInfoQuery;
import com.diy.task.bus.mapper.AnswerRecordMapper;
import com.diy.task.bus.entity.TaskContent;
import com.diy.task.bus.mapper.QuestionContentMapper;
import com.diy.task.bus.mapper.TaskCommitRecordMapper;
import com.diy.task.bus.mapper.TaskContentMapper;
import com.diy.task.bus.query.TaskCommitRecordQuery;
import com.diy.task.bus.service.ITaskCommitRecordService;
import com.diy.task.bus.service.ITaskContentService;
import com.diy.task.common.base.SmartPage;
import com.diy.task.common.dal.Condition;
import com.diy.task.common.utils.Constant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 提交记录表 服务实现类
 * </p>
 *
 * @author youwei
 * @since 2019-03-07
 */

@Service
public class TaskCommitRecordServiceImpl extends ServiceImpl<TaskCommitRecordMapper, TaskCommitRecord> implements ITaskCommitRecordService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskCommitRecordServiceImpl.class);

    @Resource
    private TaskCommitRecordMapper taskCommitRecordMapper;

    @Resource
    private TaskContentMapper taskContentMapper;

    @Resource
    private QuestionContentMapper questionContentMapper;

    @Resource
    private AnswerRecordMapper answerRecordMapper;

    @Resource
    private ITaskContentService iTaskContentService;

    /**
     * 查询提交记录，待详情，数量控制在10条内
     * @param spage
     * @return
     */
    @Override
    public List<TaskCommitRecordDTO> getTaskCommitRecordInfoList(SmartPage<TaskCommitRecordInfoQuery> spage){
        TaskCommitRecordInfoQuery taskCommitRecordInfoQuery = spage.getSearch();
        QueryWrapper<TaskCommitRecordInfoQuery> queryQueryWrapper = new QueryWrapper<>();

        // 构造查询条件，待优化
        if(taskCommitRecordInfoQuery != null && StringUtils.isNotBlank(taskCommitRecordInfoQuery.getTaskContentId())){
            queryQueryWrapper.eq("task_content_id",taskCommitRecordInfoQuery.getTaskContentId());
        }
        if(taskCommitRecordInfoQuery != null && StringUtils.isNotBlank(taskCommitRecordInfoQuery.getUserName())){
            queryQueryWrapper.like("u.user_name",taskCommitRecordInfoQuery.getUserName());
        }
        if(taskCommitRecordInfoQuery != null &&
                taskCommitRecordInfoQuery.getBeginDate() != null &&
                taskCommitRecordInfoQuery.getEndDate() != null){
            queryQueryWrapper.between("commit_date",taskCommitRecordInfoQuery.getBeginDate(),taskCommitRecordInfoQuery.getBeginDate());
        }

        IPage<TaskCommitRecordDTO> recordListPage = taskCommitRecordMapper.selectTaskCommitList(spage, queryQueryWrapper);
        List<TaskCommitRecordDTO> recordList = recordListPage.getRecords();

        if(recordList.size() > 0){
            List<String> recordIdList = recordList.stream().map(TaskCommitRecordDTO::getId).collect(Collectors.toList());
            List<AnswerRecordDTO> answerRecords = taskCommitRecordMapper.selectAnswersByCommitList(recordIdList);
            // 封装提交记录与详情map
            Map<String,Integer> recordMap = new LinkedHashMap<>((int) spage.getSize());
            for(int i = 0,size = recordList.size(); i < size; i++){
                recordList.get(i).setAnswerRecordList(new LinkedList<>());
                recordMap.put(recordList.get(i).getId(),i);
            }

            // 问题回答内容设置
            answerRecords.forEach(item -> {
                // sql排序不稳定可能会出NPE问题
                if(taskCommitRecordInfoQuery.getQuestionContentId() == null ||
                        taskCommitRecordInfoQuery.getQuestionContentId().equals(item.getQuestionId())){
                    int taskCommitId = recordMap.get(item.getTaskCommitId());
                    recordList.get(taskCommitId).getAnswerRecordList().add(item);
                }
            });
            return recordList;
        }

        return null;

    }

    @Override
    public IPage<TaskCommitRecordBO> listByPage(TaskCommitRecordQuery taskCommitRecordQuery) {

        Condition condition = Condition.create();
        String userName = taskCommitRecordQuery.getUserName();
        Integer taskNumber = taskCommitRecordQuery.getTaskNumber();
        String taskSubject = taskCommitRecordQuery.getTaskSubject();
        Date subjectStartTime = taskCommitRecordQuery.getSubjectStartTime();
        Date subjectEndTime = taskCommitRecordQuery.getSubjectEndTime();
        if (StringUtils.isNotBlank(userName)) {
            condition = condition.with("userName", userName);
        }
        if (Objects.nonNull(taskNumber)) {
            condition = condition.with("taskNumber", taskNumber);
        }
        if (StringUtils.isNotBlank(taskSubject)) {
            condition = condition.with("taskSubject", taskSubject);
        }
        if (Objects.nonNull(subjectStartTime)) {
            condition = condition.with("subjectStartTime", subjectStartTime);
        }
        if (Objects.nonNull(subjectEndTime)) {
            condition = condition.with("subjectEndTime", subjectEndTime);
        }
        condition = condition.with("offset",  taskCommitRecordQuery.getOffset()).with("pageSize", taskCommitRecordQuery.getPageSize());

        List<TaskCommitRecordBO> taskCommitRecordBOS = taskCommitRecordMapper.listTaskCommitRecordPageWithSubject(condition);

        int count = taskCommitRecordMapper.listTaskCommitRecordCountWithSubject(condition);

        IPage<TaskCommitRecordBO> taskCommitRecordIPage = new IPage<TaskCommitRecordBO>() {
            @Override
            public List<TaskCommitRecordBO> getRecords() {
                return null;
            }

            @Override
            public IPage<TaskCommitRecordBO> setRecords(List<TaskCommitRecordBO> list) {
                return null;
            }

            @Override
            public long getTotal() {
                return 0;
            }

            @Override
            public IPage<TaskCommitRecordBO> setTotal(long l) {
                return null;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public IPage<TaskCommitRecordBO> setSize(long l) {
                return null;
            }

            @Override
            public long getCurrent() {
                return 0;
            }

            @Override
            public IPage<TaskCommitRecordBO> setCurrent(long l) {
                return null;
            }
        };
        taskCommitRecordIPage.setRecords(taskCommitRecordBOS);
        taskCommitRecordIPage.setTotal(count);
        taskCommitRecordIPage.setCurrent(taskCommitRecordQuery.getOffset());
        taskCommitRecordIPage.setSize(taskCommitRecordQuery.getPageSize());
        return taskCommitRecordIPage;
    }
    /**
     * 添加作业提交
     * @param taskCommitRecord
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveTaskCommitRecord(TaskCommitRecord taskCommitRecord) {
        List<LinkedHashMap>  answerRecordList = taskCommitRecord.getAnswerRecordList();
        // 设置提交时间 todo 提交时间逻辑待增加
        taskCommitRecord.setCommitDate(LocalDateTime.now());
        taskCommitRecordMapper.insert(taskCommitRecord);

        for (LinkedHashMap map : answerRecordList) {
            AnswerRecord answerRecord = new AnswerRecord();
            answerRecord.setAnswerContext((String) map.get("answerContext"));
            answerRecord.setQuestionContent((String) map.get("questionContent"));
            answerRecord.setQuestionId((String) map.get("questionId"));
            answerRecord.setTaskCommitId(taskCommitRecord.getId());
            answerRecord.setCreator(taskCommitRecord.getCreator());
            answerRecord.setModifier(taskCommitRecord.getModifier());
            answerRecordMapper.insert(answerRecord);
        }

    }

    /**
     * 编辑作业
     * @param taskCommitRecord
     */
    @Override
    public void updateTaskCommitRecord(TaskCommitRecord taskCommitRecord) {
        List<AnswerRecord>  answerRecordList = taskCommitRecord.getAnswerRecordList();

        for (AnswerRecord answerRecord : answerRecordList) {
            answerRecord.setModifier(taskCommitRecord.getModifier());
            answerRecordMapper.updateById(answerRecord);
        }
        taskCommitRecordMapper.updateById(taskCommitRecord);
    }

    /**
     * 查询编辑作业的详情
     * @param id
     * @return
     */
    @Override
    public TaskCommitRecord getTaskCommitRecord(String id) {
        TaskCommitRecord taskCommitRecord = taskCommitRecordMapper.selectById(id);
        String taskCommitId = taskCommitRecord.getId();
        List<String> recordIdList = new ArrayList<>(1);
        recordIdList.add(taskCommitId);
        List<AnswerRecordDTO> answerRecords = taskCommitRecordMapper.selectAnswersByCommitList(recordIdList);
        taskCommitRecord.setAnswerRecordList(answerRecords);
        return taskCommitRecord;
    }

    /**
     * 获取最近一期的作业内容
     * @return
     */
    @Override
    public TaskContent selectTaskContent() {
        QueryWrapper<TaskContent> taskContentQueryWrapper = new QueryWrapper<>();
        taskContentQueryWrapper.eq("is_deleted", Constant.ZERO.getCode()).orderByDesc("task_number").last("LIMIT 1");
        TaskContent taskContent = taskContentMapper.selectOne(taskContentQueryWrapper);
        TaskContent detail = null;
        if (taskContent!=null){
            detail = iTaskContentService.getDetail(taskContent.getId());
        }
        return detail;
    }

}
