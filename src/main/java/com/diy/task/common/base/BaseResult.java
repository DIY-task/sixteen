package com.diy.task.common.base;

/**
 * @Usage: 结果集
 * @Author: gjq
 * @Date: 2019/3/4 8:33 PM
 */
public class BaseResult {
    private static final long serialVersionUID = 1L;

    private static final Integer CODE = 0;

    private Integer code;

    private String msg;

    private Object result;

    public BaseResult() {

    }

    public BaseResult(Integer code) {
        this.code = code;
    }

    public BaseResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseResult(Integer code, Object result) {
        this.code = code;
        this.result = result;
    }

    public BaseResult(Integer code,String msg, Object result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public BaseResult(Object result) {
        this.result = result;
    }

    public static BaseResult error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static BaseResult error(String msg) {
        return error(500, msg);
    }

    public static BaseResult error(Integer code, String msg) {
        return new BaseResult(code, msg);
    }

    public static BaseResult ok(String msg) {
        return new BaseResult(CODE, msg);
    }

    public static BaseResult ok(Object result) {
        return new BaseResult(CODE, result);
    }
    public static BaseResult ok(String msg, Object result) {
        return new BaseResult(CODE, msg, result);
    }
    public static BaseResult ok() {
        return new BaseResult(CODE);
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        // Create a copy, don't share the array
        return "code:"+this.getCode()+"msg:"+this.getMsg()+"result:"+this.getResult();
    }
}
