package com.diy.task.common.config;

import com.alibaba.druid.support.http.StatViewServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Usage:
 * @Author: gjq
 * @Date: 2019/3/4 5:28 PM
 */
@Configuration
public class DruidConfig {

    @Bean
    public ServletRegistrationBean druidServlet() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
        servletRegistrationBean.setServlet(new StatViewServlet());
        servletRegistrationBean.addUrlMappings("/database/*");
        servletRegistrationBean.addInitParameter("loginUsername","diy");
        servletRegistrationBean.addInitParameter("loginPassword","12345678");
        return servletRegistrationBean;
    }
}
