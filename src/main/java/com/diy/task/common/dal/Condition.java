package com.diy.task.common.dal;

import org.apache.ibatis.session.RowBounds;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 查询条件对象
 *
 * @author youwei
 * @since 2019-03-09
 */
public class Condition extends LinkedHashMap<String, Object> {
    /**
     * sid
     */
    private static final long serialVersionUID = 6348556354026088206L;

    private Condition() {
    }

    public static Condition create() {
        return new Condition();
    }

    public static Condition create(RowBounds rb) {
        Condition cond = new Condition();
        cond.put("offset", rb.getOffset());
        cond.put("limit", rb.getLimit());
        return cond;
    }

    public static Condition create(Map<String, Object> conditions) {
        Condition condition = new Condition();
        condition.putAll(conditions);
        return condition;
    }

    public Condition with(String condition, Object value) {
        this.put(condition, value);
        return this;
    }

}

