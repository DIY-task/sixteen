package com.diy.task.common.exception;

/**
 * @Description 业务异常的封装
 */
public class BussinessException extends RuntimeException{

	//友好提示的code码
	private int code;
	
	//友好提示
	private String msessage;
	
	//业务异常跳转的页面
	private String url;

	public BussinessException(String message){
		this.code = code;
		this.msessage = message;
	}

	public BussinessException(int code, String message){
		this.code = code;
		this.msessage = message;
	}

	public BussinessException(int code, String message,String url){
		this.code = code;
		this.msessage = message;
		this.url = url;
	}

	public BussinessException(BizExceptionEnum bizExceptionEnum){
		this.code = bizExceptionEnum.getCode();
		this.msessage = bizExceptionEnum.getMessage();
		this.url = bizExceptionEnum.getUrlPath();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsessage() {
		return msessage;
	}

	public void setMsessage(String msessage) {
		this.msessage = msessage;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
