package com.diy.task.common.exportbus;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Font;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.metadata.TableStyle;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.diy.task.common.exception.BizExceptionEnum;
import com.diy.task.common.exception.BussinessException;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author nengjie
 * @time 2019-03-09 9:48
 */
public class ExportUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportUtils.class);

    /**
     * 导出未提交的学员名单
     * @param httpServletResponse
     * @param fileName
     * @param unSubmittedNameList
     * @throws Exception
     */
    public static void exportUnSubmittedName(HttpServletResponse httpServletResponse, String fileName, List<String> unSubmittedNameList){
        try{
            ServletOutputStream out = getServletOutputStream(httpServletResponse, fileName);
            ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
            String sheetName = "未提交的学员名单";
            List<String> headList = Arrays.asList("序号", "姓名");
            Sheet sheet = getExcelSheet(sheetName,headList);

            // 组装要导出的数据
            List<List<Object>> object = new ArrayList<>();
            for (int i = 0; i < unSubmittedNameList.size(); i++) {
                List<Object> da = new ArrayList<>();
                da.add((i+1) + "");
                da.add(unSubmittedNameList.get(i));
                object.add(da);
            }

            writer.write1(object, sheet);
            writer.finish();
            out.flush();
        }catch (Exception e){
            LOGGER.error("导出未提交的学员名单出错",e.getMessage(),e);
            throw new BussinessException(BizExceptionEnum.EXPORT_ERROR);
        }
    }


    /**
     * 设置Sheet
     * @param sheetName
     * @return
     */
    private static Sheet getExcelSheet(String sheetName,List<String> headList) {
        Sheet sheet = new Sheet(1, 0);

        //组装要导出的数据表头
        List<List<String>> head = new ArrayList<List<String>>();
        for (String headName : headList) {
            List<String> headCoulumn = new ArrayList<String>();
            headCoulumn.add(headName);
            head.add(headCoulumn);
        }

        // 设置表头信息
        sheet.setHead(head);
        sheet.setSheetName(sheetName);
        sheet.setAutoWidth(Boolean.TRUE);
        sheet.setTableStyle(createTableStyle());
        return sheet;
    }

    /**
     * 设置表格的样式
     * @return
     */
    private static TableStyle createTableStyle() {
        TableStyle tableStyle = new TableStyle();
        Font headFont = new Font();
        headFont.setBold(true);
        headFont.setFontHeightInPoints((short)14);
        headFont.setFontName("宋体");
        tableStyle.setTableHeadFont(headFont);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.GREEN);

        Font contentFont = new Font();
        contentFont.setBold(false);
        contentFont.setFontHeightInPoints((short)12);
        contentFont.setFontName("宋体");
        tableStyle.setTableContentFont(contentFont);
        tableStyle.setTableContentBackGroundColor(IndexedColors.GREY_25_PERCENT);
        return tableStyle;
    }


    /**
     * 设置导出的表格格式
     * @param httpServletResponse
     * @param fileName
     * @return
     * @throws Exception
     */
    private static ServletOutputStream getServletOutputStream(HttpServletResponse httpServletResponse,String fileName)throws Exception{
        httpServletResponse.setContentType("multipart/form-data");
        httpServletResponse.setCharacterEncoding("utf-8");
        String encode = URLEncoder.encode(fileName, "UTF-8");
        httpServletResponse.setHeader("Content-disposition", "attachment;filename="+encode+".xlsx");
        ServletOutputStream out = httpServletResponse.getOutputStream();
        return out;
    }

}
