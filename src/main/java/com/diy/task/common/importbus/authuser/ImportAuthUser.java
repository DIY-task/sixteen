package com.diy.task.common.importbus.authuser;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

/**
 * @author nengjie
 * @time 2019-03-08 23:49
 */
public class ImportAuthUser extends BaseRowModel {

    @ExcelProperty(index = 0)
    private String userName;


    public ImportAuthUser() {
    }

    public ImportAuthUser(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
