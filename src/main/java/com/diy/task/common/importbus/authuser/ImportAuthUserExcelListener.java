package com.diy.task.common.importbus.authuser;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.diy.task.common.config.SpringManager;
import com.diy.task.common.utils.Constant;
import com.diy.task.sys.entity.AuthUser;
import com.diy.task.sys.service.IAuthUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nengjie
 * @time 2019-03-08 23:53
 */
public class ImportAuthUserExcelListener extends AnalysisEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportAuthUserExcelListener.class);

    private List<ImportAuthUser> importAuthUserList = new ArrayList<>();

    @Override
    public void invoke(Object object, AnalysisContext context) {
        if(context.getCurrentRowNum() > 0){
            importAuthUserList.add((ImportAuthUser) object);
        }
    }


    /**
     * 插入用户数据
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        List<AuthUser> authUserList = new ArrayList<>();
        LOGGER.info("导入的用户数据：{}"+ JSON.toJSONString(importAuthUserList));
        for (ImportAuthUser importAuthUser : importAuthUserList) {
            AuthUser authUser = new AuthUser();
            authUser.setRoleId(String.valueOf(1L));
            authUser.setUserName(importAuthUser.getUserName());
            authUser.setPassword("123456");
            authUser.setCommitNumber(4);
            authUser.setStatus("NORMAL");
            authUser.setDeleted(Constant.ZERO.getCode());
            authUserList.add(authUser);
        }

        IAuthUserService authUserService = SpringManager.getBean(IAuthUserService.class);
        authUserService.saveBatch(authUserList);
    }
}
