package com.diy.task.common.utils;

/**
 * 全局常量类
 */
public enum Constant {
    ZERO(0,"未删除"),
    ONE(1,"已删除");

    Constant(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }
    private Integer code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
