package com.diy.task.common.utils;

/**
 * @author nengjie
 * @time 2019-03-07 22:47
 */
public enum TaskStatusEnum {


    PROCESSING(1,"话题正在进行中"),
    NOTSTART(2,"话题还未开始"),
    OVER(3,"话题已结束");

    TaskStatusEnum(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }
    private Integer code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
