package com.diy.task.sys.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.diy.task.common.base.BaseResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.diy.task.common.base.BaseController;
import com.diy.task.sys.service.IAuthUserService;
import com.diy.task.sys.entity.AuthUser;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author hxm
 * @since 2019-03-05
 */
@Controller
@RequestMapping("/sys/auth-user")
public class AuthUserController extends BaseController<IAuthUserService,AuthUser> {

    @Autowired
    private IAuthUserService iAuthUserService;

    /**
     * 用户登录
     * @param userName
     * @param password
     * @return
     */
    @PostMapping("login")
    @ApiOperation(value = "用户登录", notes = "传入参数：用户名userName,密码password。")
    @ResponseBody
    public BaseResult login (HttpServletRequest request,
                             @ApiParam(name = "userName",value = "用户名")@RequestParam(name = "userName") String userName ,
                             @ApiParam(name = "password",value = "密码")@RequestParam(name = "password") String password){
        if (StringUtils.isEmpty(userName)||StringUtils.isEmpty(password)){
            return BaseResult.error("用户名或密码不能为空！");
        }
        return iAuthUserService.login(request,userName,password);
    }

    @PostMapping("account")
    @ApiOperation(value = "用户登录", notes = "传入参数：{用户名userName,密码password}。")
    @ResponseBody
    public BaseResult account (HttpServletRequest request, @RequestBody Map<String,String> map){
        String userName = map.get("userName");
        String password = map.get("password");
        if (StringUtils.isEmpty(userName)||StringUtils.isEmpty(password)){
            return BaseResult.error("用户名或密码不能为空！");
        }
        return iAuthUserService.login(request,userName,password);
    }

    /**
     * 导入用户数据
     */
    @GetMapping("importUserInfo")
    @ApiOperation(value = "导入用户数据", notes = "")
    public void importUserInfo (){
        iAuthUserService.importUserInfo();
    }

}
