package com.diy.task.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.diy.task.sys.entity.AuthUser;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author hxm
 * @since 2019-03-05
 */
public interface AuthUserMapper extends BaseMapper<AuthUser> {


    @Select("SELECT b.user_name FROM sys_auth_user b WHERE  b.status ='NORMAL' and  b.commit_number < " +
            "(SELECT c.task_number FROM bus_task_content c WHERE c.is_deleted = 0 ORDER BY c.task_number DESC LIMIT 1)")
    List<String> getUnSubmittedName();

}
