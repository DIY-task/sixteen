package com.diy.task.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.diy.task.common.base.BaseResult;
import com.diy.task.sys.entity.AuthUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author hxm
 * @since 2019-03-05
 */
public interface IAuthUserService extends IService<AuthUser> {

    /**
     * 导出
     */
    void export(HttpServletResponse httpServletResponse,String fileName);


    /**
     * 登录
     * @param request
     * @param userName
     * @param password
     * @return
     */
    BaseResult login(HttpServletRequest request, String userName, String password);

    /**
     * 导入用户信息
     */
    void importUserInfo();
}
